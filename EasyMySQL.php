<?php

// Упрощенный класс для работы с БД MySQL с шаблоном Singleton

namespace paperman\helpers;

class EasyMySQL extends \mysqli
{
    protected static $_instance;  //экземпляр объекта
	//private $con = false;
	//private $insert_id = false;
	private $last_sql = false;
	//private $affected_rows = false;
	//private $num_rows = false;
	//private $config = false;
	private $query_time = false;
    private $charset = 'utf8';
    
    // получить экземпляр данного класса 
    public static function getInstance(Array $config = null, $database = false)
    {
        // если экземпляр данного класса не создан
        if (self::$_instance === null){
            // создаем экземпляр данного класса
            self::$_instance = new self($config, $database); 
        }
        return self::$_instance; // возвращаем экземпляр данного класса
    }
	
	public function __construct($config, $database = false)
	{
		//$this->config = $config;
		
		// проверяем наличие обязательных для соединения с базой параметров
        foreach (array('host','login','password','database') as $param)
		{
			if (!isset($config[$param])){
                throw new \LogicException("В конфигурации отсутствует необходимый параметр [{$param}]");
            }
        }
		
        // если при создании объекта дополнительно указано название базы
        // данных используем его для подключения 
		if ($database) $config['database'] = $database;
        
        // производим подключение к базе 
		parent::__construct($config['host'],$config['login'],$config['password'],$config['database']);
		
        if($this->connect_errno)
		{
			throw new \RuntimeException("Ошибка подключения к базе данных MySQL: [{$this->connect_error}]");
		}
        
        // используем параметр с кодировкой если есть в конфигурации
        if (@$config['charset']) $this->charset = $config['charset'];
        $this->set_charset($this->charset);
	}

	// обычный запрос
    public function select($sql,$assoc = 1)
	{
		$this->last_sql = $sql;
		$resultType = array(MYSQLI_NUM,MYSQLI_ASSOC);
		$msc = microtime(true);

		if (!$result = $this->query($sql)) $this->query_error($sql);
		//$this->num_rows = $result->num_rows;
		$this->query_time = number_format(microtime(true) - $msc,3,'.','');

		while ($row = $result->fetch_array($resultType[$assoc])) $rows[] = $row;
		if (isset($rows)) return $rows;
		else return FALSE;
	}

    // запрос с возвратом только первой строки в виде массива
	public function select_single_row($sql,$assoc = 1)
	{
		$rows = $this->select($sql,$assoc);
		if ($rows) return $rows[0];
		return FALSE;
	}

    // запрос с возвратом первой колонки в виде массива
	public function select_single_column($sql)
	{
		$rows = $this->select($sql,0);
		if (!$rows) return FALSE;
		$result = array();
		foreach($rows as $row) {
			$result[] = $row[0];
		}
		return $result;
	}

    // запрос с возвратом скалярного значения
	public function select_single_value($sql)
	{
		list($value) = $this->select_single_row($sql,0);
		if ($value) return $value;
		return FALSE;
	}

    // вставка
	public function insert($sql)
	{
		$this->last_sql = $sql;
		$msc = microtime(true);
		
		if (!$result = $this->query($sql)) $this->query_error($sql);
		//$this->insert_id = $this->con->insert_id;
		//$this->affected_rows = $this->con->affected_rows;
		$this->query_time = number_format(microtime(true) - $msc,3,'.','');
		return TRUE;
	}

    // обновление
	public function update($sql)
	{
		$this->last_sql = $sql;
		$msc = microtime(true);

		if (!$result = $this->query($sql)) $this->query_error($sql);
		//$this->affected_rows = $this->con->affected_rows;
		$this->query_time = number_format(microtime(true) - $msc,3,'.','');
		return TRUE;
	}
    
    // переключение автоматическго завершения транзакции
    // public function autocommit(bool $val = true)
    // {
        // $this->con->autocommit($val);
    // }
    
    // запуск транзакции
    // public function begin_transaction()
    // {
        // $this->con->begin_transaction();
    // }
    
    // завершение транзакции
    // public function commit()
    // {
        // $this->con->commit();
    // }

    // откат транзакции
    // public function rollback()
    // {
        // $this->con->rollback();
    // }
    
    // реакия на ошибку в запросе к базе
	private function query_error($sql)
	{
		throw new \RuntimeException("На запрос: [{$sql}] база MySQL ответила: [{$this->error}]");
	}

	// разрешенные для чтения свойства объекта
    public function __get($name)
	{
		$readable_vars = array('insert_id','last_sql','affected_rows','num_rows','query_time');
		if(in_array($name,$readable_vars)) return $this->$name;
		else throw new \LogicException("Свойство \"{$name}\" защищено от чтения.");
	}
    
    private function __clone() //запрещаем клонирование объекта модификатором private
    { 
    }
    
    private function __wakeup() //запрещаем клонирование объекта модификатором private
    {
    }
}