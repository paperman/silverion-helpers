<?php
namespace paperman\helpers;

// Класс для настройки Smarty

class LoadSmarty extends \Smarty
{
	public function __construct(Array $config = [])
	{
        parent::__construct();
        
        $root = __DIR__ .'/../../../smarty';
        
        $this->setTemplateDir("{$root}/templates/");
        $this->setCompileDir("{$root}/templates_c/");
        $this->setConfigDir("{$root}/configs/");
        $this->setCacheDir("{$root}/cache/");
        $this->addPluginsDir("{$root}/plugins/");
        
        foreach ($config as $key => $value)
        {
            if (property_exists($this, $key)) $this->$key = $value;
        }
	}
}